/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.1789650215620508, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.7075, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [0.0, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.0, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [0.005, 500, 1500, "suggestProgram"], "isController": false}, {"data": [0.4897959183673469, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.037383177570093455, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.014018691588785047, 500, 1500, "findAllClass"], "isController": false}, {"data": [0.20297029702970298, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.5263157894736842, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [0.5151515151515151, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [0.45495495495495497, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.0, 500, 1500, "registrationByID"], "isController": false}, {"data": [0.0, 500, 1500, "leadByID"], "isController": false}, {"data": [0.014423076923076924, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.0, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.0, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [0.0391304347826087, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [0.0, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2087, 0, 0.0, 39379.49592716824, 3, 334320, 15086.0, 98075.8, 136131.3999999999, 323898.36, 5.9626527090499755, 37.44919035811629, 11.98951359614099], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 200, 0, 0.0, 1390.7049999999997, 3, 17582, 67.0, 5271.2, 7416.799999999998, 8063.56, 0.6743611271271879, 0.3971091402907171, 0.8322860979189216], "isController": false}, {"data": ["getCentreHolidaysOfYear", 93, 0, 0.0, 31455.215053763437, 5115, 111650, 25077.0, 56244.6, 74483.2, 111650.0, 0.28932574656931215, 0.766668264309958, 0.3715462468150835], "isController": false}, {"data": ["getPastChildrenData", 101, 0, 0.0, 85937.03960396035, 42660, 202907, 78538.0, 123667.59999999999, 142852.29999999993, 202675.24000000005, 0.29599064552336124, 0.5908193537586417, 0.6749396067353991], "isController": false}, {"data": ["suggestProgram", 100, 0, 0.0, 17112.580000000005, 1272, 82375, 13681.0, 39608.50000000001, 44901.799999999974, 82041.44999999984, 0.3135248343021251, 0.25412657467848027, 0.38915045351367283], "isController": false}, {"data": ["findAllWithdrawalDraft", 98, 0, 0.0, 2452.3367346938776, 17, 20495, 891.0, 7811.8, 8422.599999999997, 20495.0, 0.34935743186638857, 0.22619529035880434, 1.1405291940716182], "isController": false}, {"data": ["findAllLevels", 107, 0, 0.0, 9212.766355140187, 87, 39900, 6931.0, 18468.800000000003, 31864.99999999998, 39773.12, 0.373134328358209, 0.23758162313432837, 0.31264575559701496], "isController": false}, {"data": ["findAllClass", 107, 0, 0.0, 13851.953271028033, 547, 75221, 8582.0, 33421.0, 46042.999999999985, 74713.40000000001, 0.34284322772480263, 34.14805598177323, 0.2772208911681021], "isController": false}, {"data": ["getChildrenToAssignToClass", 101, 0, 0.0, 6261.643564356434, 70, 42127, 3428.0, 15592.999999999995, 23215.399999999947, 42096.44, 0.32968180809254594, 0.18769970128706473, 0.24178812292724805], "isController": false}, {"data": ["getStaffCheckInOutRecords", 95, 0, 0.0, 2248.9578947368414, 6, 20894, 978.0, 6456.200000000002, 7969.799999999999, 20894.0, 0.32106417565927997, 0.19031831506365518, 0.4201425736166358], "isController": false}, {"data": ["getCountStaffCheckInOut", 99, 0, 0.0, 2171.0707070707062, 23, 26229, 758.0, 6222.0, 7251.0, 26229.0, 0.3307342299905791, 0.1954045011174808, 0.33719388292008257], "isController": false}, {"data": ["searchBroadcastingScope", 102, 0, 0.0, 101480.23529411767, 50238, 177399, 97724.0, 128393.40000000001, 156043.24999999994, 177242.91, 0.29171445159113074, 0.5651827853796721, 0.571748930022851], "isController": false}, {"data": ["getTransferDrafts", 111, 0, 0.0, 3854.612612612612, 15, 37314, 1290.0, 9740.799999999996, 15169.59999999998, 37139.399999999994, 0.372340782385262, 0.2392580418061547, 0.646142158494737], "isController": false}, {"data": ["registrationByID", 86, 0, 0.0, 48706.31395348836, 4036, 136912, 44497.5, 79090.0, 96689.19999999997, 136912.0, 0.2658727582443741, 0.4082317036647159, 1.2094094803733348], "isController": false}, {"data": ["leadByID", 112, 0, 0.0, 41741.857142857145, 8367, 136593, 38659.5, 72511.0, 82413.59999999999, 130948.5300000002, 0.3591021164580989, 0.8593896536508181, 1.6215704946311027], "isController": false}, {"data": ["getRegEnrolmentForm", 104, 0, 0.0, 24332.451923076922, 807, 83628, 19005.5, 47793.5, 66998.5, 83230.30000000002, 0.3375461691755435, 0.6075647210294509, 1.4431417271977827], "isController": false}, {"data": ["findAllLeads", 119, 0, 0.0, 73379.89915966385, 37533, 122890, 71832.0, 105693.0, 113967.0, 122697.8, 0.34691652430456354, 0.6445269226536491, 0.7853051790409945], "isController": false}, {"data": ["getEnrollmentPlansByYear", 145, 0, 0.0, 188114.87586206896, 43947, 334320, 175912.0, 326593.4, 330775.8, 333677.83999999997, 0.42671155476033523, 0.47503847761088613, 0.7079911440798922], "isController": false}, {"data": ["getAvailableVacancy", 115, 0, 0.0, 15452.77391304348, 133, 99786, 10604.0, 32127.8, 44895.59999999999, 95037.3600000001, 0.37610337282963824, 0.20604882046623738, 0.4976758497892186], "isController": false}, {"data": ["getRegistrations", 92, 0, 0.0, 52849.78260869564, 22431, 121285, 49869.0, 80935.2, 90644.94999999995, 121285.0, 0.2820805278585182, 0.606182095628365, 0.8710338174693697], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2087, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
